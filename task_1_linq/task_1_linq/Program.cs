﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.Design;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace task_1_linq
{
    class Program
    {
        public class Project
        {
            [JsonProperty("id")]
            public int Id { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("description")]
            public string Description { get; set; }
            [JsonProperty("createdAt")]
            public string CreatedAt { get; set; }
            [JsonProperty("deadline")]
            public string Deadline { get; set; }
            [JsonProperty("authorId")]
            public int AuthorId { get; set; }
            [JsonProperty("teamId")]
            public int TeamId { get; set; }


        }

        public enum State { }

        public class Task
        {
            [JsonProperty("id")]
            public int Id { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("description")]
            public string Description { get; set; }
            [JsonProperty("createdAt")]
            public string CreatedAt { get; set; }
            [JsonProperty("finishedAt")]
            public string FinishedAt { get; set; }
            [JsonProperty("projectId")]
            public int ProjectId { get; set; }
            [JsonProperty("performerId")]
            public int PerformerId { get; set; }
            [JsonProperty("state")]
            public int State { get; set; }

        }

        public class Team
        {
            [JsonProperty("id")]
            public int Id { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("createdAt")]
            public string CreatedAt { get; set; }
        }

        public class User
        {
            [JsonProperty("id")]
            public int Id { get; set; }
            [JsonProperty("firstName")]
            public string FirstName { get; set; }
            [JsonProperty("lastName")]
            public string LastName { get; set; }
            [JsonProperty("email")]
            public string Email { get; set; }
            [JsonProperty("birthday")]
            public string Birthday { get; set; }
            [JsonProperty("registeredAt")]
            public string RegisteredAt { get; set; }
            [JsonProperty("teamId")]
            public int? TeamId { get; set; }
        }

        public class TaskState
        {
            public int Id { get; set; }
            public string Value { get; set; }
        }



        static public async Task<List<Project>> GetProjects()
        {
            HttpClient client = new HttpClient();
            var users = await client.GetStringAsync("https://bsa20.azurewebsites.net/api/Projects");
            return JsonConvert.DeserializeObject<List<Project>>(users);
        }

     
        static public async Task<List<Task>> GetTasks()
        {
            HttpClient client = new HttpClient();
            var users = await client.GetStringAsync("https://bsa20.azurewebsites.net/api/Tasks");
            return JsonConvert.DeserializeObject<List<Task>>(users);
        }


        static public async Task<List<Team>> GetTeams()
        {
            HttpClient client = new HttpClient();
            var users = await client.GetStringAsync("https://bsa20.azurewebsites.net/api/Teams");
            return JsonConvert.DeserializeObject<List<Team>>(users);
        }

        static public async Task<List<User>> GetUsers()
        {
            HttpClient client = new HttpClient();
            var users = await client.GetStringAsync("https://bsa20.azurewebsites.net/api/Users");
            return JsonConvert.DeserializeObject<List<User>>(users);
        }

       

        static public Dictionary<int, int> NumberOfTasksOfUser(int id)
        { 
            return (from pr in GetProjects().Result
                        join t in GetTasks().Result on pr.Id equals t.ProjectId
                        where pr.AuthorId == id
                        group t by pr.Id into k
                        select new { k.Key, Value = k.Count() }).ToDictionary(a => a.Key, a => a.Value);
          
        }

       
        static public List<Task> ListOfTaskBelongedToUser(int id)
        {
            return GetTasks().Result.Where(x => (x.Name.Length < 45) && (x.PerformerId == id)).ToList();           
        }

        static public List<Tuple<int,string>> ListOfTaskThatAreDoneTillThisYear(int id)
        {
           var ta = (from t in GetTasks().Result
                      where t.PerformerId == id
                      && t.FinishedAt.Contains("2020")
                      select new Tuple<int,string>( t.Id, t.Name )).ToList();
            return ta;
        }

        static public List<Tuple<User, List<Task>>> ListOfUsersOnAlphabetWithTasks()
        {
            var ta = (from user in GetUsers().Result
                      join t in GetTasks().Result on user.Id equals t.PerformerId into tas
                      orderby user.FirstName ascending
                      select new Tuple<User, List<Task>>
                      (
                          user,
                          ( from tass in tas
                                orderby tass.Name.Length descending
                                select tass).ToList()
                      )).ToList();

            return ta;
        }

        static public List<Tuple<int, string, List<User>>> ListOfTeamsWithMemembersOlderThanTenYears()
        { 
            var ta = (from team in GetTeams().Result
                      join user in GetUsers().Result on team.Id equals user.TeamId
                      where (2020 - Int32.Parse(user.Birthday.Remove(4))) > 10
                      orderby user.RegisteredAt descending
                      group user by new { team.Id, team.Name } into q
                      select new Tuple<int,string,List<User>>
                      (
                          q.Key.Id,
                          q.Key.Name,
                          q.ToList()
                      )).ToList();
            
            return ta;
        }


        public class UserSpecial
        {
            public User User { get; set; }
            public Project Project { get; set; }
            public int TaskNotDone { get; set; }
           
        }


        public class ProjectSpecial
        {
            public Project Project { get; set; }
            public Task TaskLong { get; set; }
            public Task TaskShort { get; set; }
            public int Users { get; set; }
        }



        static public UserSpecial GetStructureOfUsers(int id)
        { 
            var ta = (from user in GetUsers().Result
                      join pr in GetProjects().Result on user.Id equals pr.AuthorId
                      join t in GetTasks().Result on pr.Id equals t.ProjectId
                      where ((user.Id == id) && (pr.CreatedAt == (from p in GetProjects().Result where (p.AuthorId == user.Id) group p by user into projec select projec.Max(x => x.CreatedAt)).ToString()))
                      group pr by user into proj
                      select new UserSpecial
                      {
                          User = proj.Key,
                          Project = (from p in GetProjects().Result where p.CreatedAt == proj.Max(x => x.CreatedAt) select p).ToList()[0],
                          TaskNotDone = (from t in GetTasks().Result
                                   where t.PerformerId == proj.Key.Id && t.State != 2
                                   select t).Count()
                      }).ToList()[0];
            return ta;

        }

        static public List<ProjectSpecial> GetStructureOfProjects()
        {
            var taw = (from pr in GetProjects().Result
                       join t in GetTasks().Result on pr.Id equals t.ProjectId
                       group t by pr into tasks
                       select new ProjectSpecial
                       {
                           Project = tasks.Key,
                           TaskLong = (from t in GetTasks().Result where t.Description.Length == tasks.Max(x => x.Description.Length) select t).ToList()[0],
                           TaskShort = (from t in GetTasks().Result where t.Name.Length == tasks.Min(x => x.Name.Length) select t).ToList()[0],
                           Users = (from us in GetUsers().Result
                                    where us.TeamId == tasks.Key.TeamId && (tasks.Key.Description.Length > 20 || tasks.Count() < 3)
                                    select us).Count()
                       }).ToList();

            return taw;
        }



        static void Main(string[] args)
        {


        }
    }
}
